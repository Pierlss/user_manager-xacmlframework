package eu.specs.user_manager.XACML_framework.XACML_rest_client;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import eu.specs.user_manager.XACML_framework.XACML_engine.PEP;


@Controller
public class XACML_controller {

	@RequestMapping(value="/login",method=RequestMethod.POST)
	@ResponseBody
	public String receiveReq(@RequestBody String body){
		String dest;
		User u=StringToUser(body);
		
		System.out.println("[XACML Framework] Request received: "+u.getUsername()+" "+u.getRole());
		//Attivazione meccanismo di XACML ed invio risposta
		if(PEP.doFilter(u)=="PERMIT"){
			if(u.getRole().equals("user")==true){
				System.out.println("[XACML Framework] User page");
				dest="reserved/user";
			}
			else if(u.getRole().equals("developer")==true){
				System.out.println("[XACML Framework] Developer page");
				dest="reserved/developer";
			}
			else if(u.getRole().equals("owner")==true){
				System.out.println("[XACML Framework] Owner page");
				dest="reserved/owner";
			}else{
				dest="errorPage";
			}
		}else{
			dest="errorPage";
		}
		return dest;
	}
	
	private User StringToUser(String s){
		String u="",pwd="",role="";
		int count=1;
		while(s.charAt(count)!=','){
			u+=s.charAt(count);
			count++;
		}
		count++;
		while(s.charAt(count)!=','){
			pwd+=s.charAt(count);
			count++;
		}
		count++;
		while(s.charAt(count)!='}'){
			role+=s.charAt(count);
			count++;
		}
		
		return new User(u,pwd,role);
	}
}
