package eu.specs.user_manager.XACML_framework.XACML_engine;

import java.io.File;
import java.io.IOException;
import java.util.*;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.*;
import com.sun.xacml.PDP;
import com.sun.xacml.PDPConfig;
import com.sun.xacml.ctx.RequestCtx;
import com.sun.xacml.ctx.ResponseCtx;
import com.sun.xacml.ctx.Result;
import com.sun.xacml.finder.AttributeFinder;
import com.sun.xacml.finder.PolicyFinder;
import com.sun.xacml.finder.impl.CurrentEnvModule;
import com.sun.xacml.finder.impl.FilePolicyModule;

import eu.specs.user_manager.XACML_framework.XACML_rest_client.User;

public class PEP{
	static File[] listFile;
	
	//path of policies folder
    //TODO: uncomment String for Windows or Linux, depending on
    //	the operating system used
        	
    //1) for Windows (policy directory into C): 
    static String PATH_POLICY = "c://policy";
	        
    //2) or Linux (policy directory into /home):
    //static String PATH_POLICY = "/home/policy";
    
    public PEP(){}
    
    public static String doFilter(User u){
    	//Restiuisce la risposta XACML: 
    	// PERMIT, DENY, INDETERMINATE o NOT APPLICABLE
    	String response="";
    	//System.out.println("[XACML Framework] PEP filter");
		File f;
        String policyfile;
        FilePolicyModule policyModule = new FilePolicyModule();
        PolicyFinder policyFinder = new PolicyFinder();
        Set<FilePolicyModule> policyModules = new HashSet<FilePolicyModule>();
        
        if(u.getUsername()!=null){
        	
	        searchPolicyFile (new File(PATH_POLICY));
	        System.out.println("[XACML Framework] Policies found:");
	        for(int i=0;i<listFile.length;i++)
	        {
	        	 //File f = new File(PATH_POLICY);
	             f=listFile[i];
	             System.out.println("\t"+f.toString());
	             policyfile = f.getAbsolutePath();
	             policyModule.addPolicy(policyfile);
	             policyModules.add(policyModule);
	             policyFinder.setModules(policyModules);
	        	}
	        
	        CurrentEnvModule envModule = new CurrentEnvModule();
	        AttributeFinder attrFinder = new AttributeFinder();
	        List<CurrentEnvModule> attrModules = new ArrayList<CurrentEnvModule>();
	        attrModules.add(envModule);
	        attrFinder.setModules(attrModules);
	        RequestCtx XACMLrequest=null;
        	
	        try {
		        
		        XACMLrequest=RequestBuilder.createXACMLRequest(u,"http://localhost:8080/WebInterface/reserved/.*");
		        
		        //System.out.println("in PEP again: "+XACMLrequest.hashCode());
		        //System.out.println("Request sended:");
		        //XACMLrequest.encode(System.out);
		        
		        //Sending request to PDP and handle the response
		        PDP pdp = new PDP(new PDPConfig(attrFinder, policyFinder, null));
		        
		        ResponseCtx XACMLresponse = pdp.evaluate(XACMLrequest);
		        //System.out.println("Response received:");
		        //XACMLresponse.encode(System.out);
		        Set<?> ris_set = XACMLresponse.getResults();
		        
		        Result ris = null;
		        Iterator<?> it = ris_set.iterator();
		
		        while (it.hasNext()) {
		            ris = (Result) it.next();
		        }
		        int dec = ris.getDecision();
		        
		        if (dec == 0) {//PERMIT
		        	System.out.println("[XACML Framework] Response "+dec+": PERMIT");
		            response="PERMIT";
		        } else if (dec == 1) {//DENY
		        	System.out.println("[XACML Framework] Response "+dec+": DENY");
		        	response="DENY";
		        } else if (dec == 2){//INDETERMINATE
		        	System.out.println("[XACML Framework] Response "+dec+": INDETERMINATE");
		        	response="INDETERMINATE";
		        } else if (dec==3) {//NOT APPLICABLE
		        	System.out.println("[XACML Framework] Response "+dec+": NOT APPLICABLE");
		        	response="NOT APPLICABLE";
		        }
		        	
		     }
		     catch (Exception ex) {
		    	//res.sendRedirect(req.getContextPath()+"/errorAuth.jsp"); 
		    	response="ERROR";
		        ex.printStackTrace();
		    }
	        
        }
        return response;
    }
    
    private static void searchPolicyFile (File dir) {
	       
	      listFile = dir.listFiles();
	      
	  }
}
/*
public class PEP implements Filter{
	
	File[] listFile;
	
	//path of policies folder
    //TODO: uncomment String for Windows or Linux, depending on
    //	the operating system used
        	
    //1) for Windows (policy directory into C): 
    static String PATH_POLICY = "c://policy";
	        
    //2) or Linux (policy directory into /home):
    //static String PATH_POLICY = "/home/policy";
	
	public PEP(){
		
	}
	
	@Override
	public void init(FilterConfig arg0) throws ServletException {
		System.out.println("PEP filter init");
		
	}
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) 
					throws IOException, ServletException {
		System.out.println("2)PEP filter Activated");
		File f;
        String policyfile;
        FilePolicyModule policyModule = new FilePolicyModule();
        PolicyFinder policyFinder = new PolicyFinder();
        Set<FilePolicyModule> policyModules = new HashSet<FilePolicyModule>();
        
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        
        System.out.println("in PEP:");
        System.out.println(" username: "+req.getParameter("username"));
        System.out.println(" password: "+req.getParameter("password"));
        System.out.println(" role: "+req.getParameter("role"));
        
        
        if(req.getParameter("username")!=null){
        	
	        searchPolicyFile (new File(PATH_POLICY));
	        System.out.println("Policies found:");
	        for(int i=0;i<listFile.length;i++)
	        {
	        	 //File f = new File(PATH_POLICY);
	             f=listFile[i];
	             System.out.println(f.toString());
	             policyfile = f.getAbsolutePath();
	             policyModule.addPolicy(policyfile);
	             policyModules.add(policyModule);
	             policyFinder.setModules(policyModules);
	        }
	
	        CurrentEnvModule envModule = new CurrentEnvModule();
	        AttributeFinder attrFinder = new AttributeFinder();
	        List<CurrentEnvModule> attrModules = new ArrayList<CurrentEnvModule>();
	        attrModules.add(envModule);
	        attrFinder.setModules(attrModules);
	        RequestCtx XACMLrequest=null;
	        
	        try {
	        
	        XACMLrequest=RequestBuilder.createXACMLRequest(req);
	        
	        //System.out.println("in PEP again: "+XACMLrequest.hashCode());
	        //System.out.println("Request sended:");
	        //XACMLrequest.encode(System.out);
	        
	        //Sending request to PDP and handle the response
	        PDP pdp = new PDP(new PDPConfig(attrFinder, policyFinder, null));
	        
	        ResponseCtx XACMLresponse = pdp.evaluate(XACMLrequest);
	        //System.out.println("Response received:");
	        //XACMLresponse.encode(System.out);
	        Set<?> ris_set = XACMLresponse.getResults();
	        
	        Result ris = null;
	        Iterator<?> it = ris_set.iterator();
	
	        while (it.hasNext()) {
	            ris = (Result) it.next();
	        }
	        int dec = ris.getDecision();
	        
	        if (dec == 0) {//PERMIT
	        	System.out.println("Response "+dec+": PERMIT");
	            chain.doFilter(request, response);
	        } else if (dec == 1) {//DENY
	        	System.out.println("Response "+dec+": DENY");
	            res.sendRedirect(req.getContextPath()+"/errorAuth.jsp");
	        } else if (dec == 2){//INDETERMINATE
	        	System.out.println("Response "+dec+": INDETERMINATE");
	        	res.sendRedirect(req.getContextPath()+"/errorAuth.jsp"); 
	        } else if (dec==3) {//NOT APPLICABLE
	        	System.out.println("Response "+dec+": NOT APPLICABLE");
	            res.sendRedirect(req.getContextPath()+"/errorAuth.jsp"); 
	        }
	        	
	     }
	     catch (Exception ex) {
	    	res.sendRedirect(req.getContextPath()+"/errorAuth.jsp"); 
	        ex.printStackTrace();
	    }
        
        }
        
        else res.sendRedirect(req.getContextPath()+"/errorAuth.jsp");
        
        
	}

	
	
	@Override
	public void destroy() {
		
	}
	
	private void searchPolicyFile (File dir) {
	       
	      listFile = dir.listFiles();
	      
	  }

}
*/