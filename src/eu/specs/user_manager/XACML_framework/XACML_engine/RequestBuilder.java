package eu.specs.user_manager.XACML_framework.XACML_engine;

import com.sun.xacml.EvaluationCtx;
import com.sun.xacml.attr.StringAttribute;
import com.sun.xacml.ctx.Attribute;
import com.sun.xacml.ctx.RequestCtx;
import com.sun.xacml.ctx.Subject;

import eu.specs.user_manager.XACML_framework.XACML_rest_client.User;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;

public class RequestBuilder {
	static final String ACTION_IDENTIFIER = "urn:oasis:names:tc:xacml:1.0:action:action-id";
	static final String RESOURCE_IDENTIFIER = "urn:oasis:names:tc:xacml:1.0:resource:resource-id";
	static final String SUBJECT_IDENTIFIER = "urn:oasis:names:tc:xacml:1.0:subject:subject-id";
	static final String SUBJECT_ROLE_IDENTIFIER = "urn:oasis:names:tc:xacml:1.0:subject:role";
	
	public static Set<Subject> setupSubjects(User request) throws URISyntaxException {
        HashSet<Attribute> attributes = new HashSet<Attribute>();
        HashSet<Subject> subjects = new HashSet<Subject>();
        
        String role=request.getRole();
        //System.out.println("[XACML Framework] in ReqBuilder: "+role);
        if(role==null){
            subjects=null;
        }
        else{
                      
        attributes.add(new Attribute(new URI(SUBJECT_ROLE_IDENTIFIER), null, null, new StringAttribute(role)));  
        subjects.add(new Subject(attributes));
        }
        return subjects;
    }
	
	 public static Set<Attribute> setupResource(String url) throws URISyntaxException {
	        HashSet<Attribute> resource = new HashSet<Attribute>();
	        //StringAttribute value= new StringAttribute("");
	        
	        StringAttribute value= new StringAttribute(url);
	        //StringAttribute value = new StringAttribute("http://localhost:8080/WebInterface/reserved/.*");
	        
	        resource.add(new Attribute(new URI(EvaluationCtx.RESOURCE_ID),null, null, value));
	        return resource;
	    }

	    
	    public static Set<Attribute> setupAction() throws URISyntaxException {
	        HashSet<Attribute> action = new HashSet<Attribute>();
	        URI actionId = new URI(ACTION_IDENTIFIER);
	        // create the action
	        action.add(new Attribute(actionId, null, null,new StringAttribute("view")));
	        return action;
	    }
	    
	    public static Set<Attribute> setupEnvironment() throws URISyntaxException{
	    	HashSet<Attribute> env= new HashSet<Attribute>();
	    	return env;
	    }
	
	    public static RequestCtx createXACMLRequest(User request,String url) throws Exception{
	        RequestCtx XACMLrequest=null;
	        try{
	        XACMLrequest = new RequestCtx(setupSubjects(request), setupResource(url),setupAction(), setupEnvironment());
	        
	        //System.out.println("[XACML Framework] XACML Request built");
	        //System.out.println(XACMLrequest.getSubjects());
	        //System.out.println(XACMLrequest.getResource());
	        //System.out.println(XACMLrequest.getAction());
	        }catch(Exception e){
	      	  System.err.println("[XACML Framework] XACML RequestBuilder fail!!");
	      	  e.printStackTrace();
	        }
	        return XACMLrequest;
	        
	      }
	
}

/*
public class RequestBuilder {
  static final String ACTION_IDENTIFIER = "urn:oasis:names:tc:xacml:1.0:action:action-id";
  static final String RESOURCE_IDENTIFIER = "urn:oasis:names:tc:xacml:1.0:resource:resource-id";
  static final String SUBJECT_IDENTIFIER = "urn:oasis:names:tc:xacml:1.0:subject:subject-id";
  static final String SUBJECT_ROLE_IDENTIFIER = "urn:oasis:names:tc:xacml:1.0:subject:role";  
  //static final String SUBJECT_ROLE_IDENTIFIER="role"; 
  //static final String CURRENT_TIME_IDENTIFIER = "urn:oasis:names:tc:xacml:1.0:environment:current-time";
  //static final String CURRENT_TIME = "urn:oasis:names:tc:xacml:1.0:environment:current-time";
  //static final String CURRENT_DATE = "urn:oasis:names:tc:xacml:1.0:environment:current-date";
  //static final String CURRENT_DATETIME = "urn:oasis:names:tc:xacml:1.0:environment:current-dateTime";
  
  public static Set<Subject> setupSubjects(HttpServletRequest request) throws URISyntaxException {
        HashSet<Attribute> attributes = new HashSet<Attribute>();
        HashSet<Subject> subjects = new HashSet<Subject>();
        
        String role=request.getParameter("role");
        //System.out.println("in ReqBuilder: "+role);
        if(role==null){
            subjects=null;
        }
        else{
                      
        attributes.add(new Attribute(new URI(SUBJECT_ROLE_IDENTIFIER), null, null, new StringAttribute(role)));  
        subjects.add(new Subject(attributes));
        
        }
        return subjects;
    }

  
    public static Set<Attribute> setupResource(HttpServletRequest request) throws URISyntaxException {
        HashSet<Attribute> resource = new HashSet<Attribute>();
        //StringAttribute value= new StringAttribute("");
        
        StringAttribute value = new StringAttribute("http://localhost:8080/specs-apps_manager/reserved/.*");
        
        resource.add(new Attribute(new URI(EvaluationCtx.RESOURCE_ID),null, null, value));
        return resource;
    }

    
    public static Set<Attribute> setupAction() throws URISyntaxException {
        HashSet<Attribute> action = new HashSet<Attribute>();
        URI actionId = new URI(ACTION_IDENTIFIER);
        // create the action
        action.add(new Attribute(actionId, null, null,new StringAttribute("view")));
        return action;
    }
    
    public static Set<Attribute> setupEnvironment() throws URISyntaxException{
    	HashSet<Attribute> env= new HashSet<Attribute>();
    	return env;
    }
    
    public static RequestCtx createXACMLRequest(HttpServletRequest request) throws Exception{
      RequestCtx XACMLrequest=null;
      try{
      XACMLrequest = new RequestCtx(setupSubjects(request), setupResource(request),setupAction(), setupEnvironment());
      
      System.out.println("XACML Request built");
      //System.out.println(XACMLrequest.getSubjects());
      //System.out.println(XACMLrequest.getResource());
      //System.out.println(XACMLrequest.getAction());
      }catch(Exception e){
    	  System.err.println("XACML RequestBuilder fail!!");
    	  e.printStackTrace();
      }
      return XACMLrequest;
      
    }

}
*/